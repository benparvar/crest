-- usuario
insert into usuario (id, ativo, cpf, endereco, nascimento, nome, sobrenome)
values (1, true, '276.193.418-03', 'Rua Prof. Geraldina da Silva R. P. 582, Almerinda Chaves, Jundiaí - SP', TO_TIMESTAMP('01/10/1980', 'MM/dd/yyyy'), 'Alan', 'Alencar da Silva');

insert into usuario (id, ativo, cpf, endereco, nascimento, nome, sobrenome)
values (2, true, '907.839.890-67', 'Rua Prof. Geraldina da Silva R. P. 582, Almerinda Chaves, Jundiaí - SP', TO_TIMESTAMP('01/10/1983', 'MM/dd/yyyy'), 'Alone', 'Alencar da Silva');

-- usuario_emails
insert into usuario_emails (usuario_id, emails)
values (1, 'benparvar@gmail.com');

insert into usuario_emails (usuario_id, emails)
values (1, 'oompawampa@gmail.com');

insert into usuario_emails (usuario_id, emails)
values (2, 'kabul@gmail.com');

-- usuario_telefones
insert into usuario_telefones (usuario_id, telefones)
values (1, '(11) 94379-3198');

insert into usuario_telefones (usuario_id, telefones)
values (2, '(11) 98079-3199');

