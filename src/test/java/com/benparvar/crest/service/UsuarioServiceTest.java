package com.benparvar.crest.service;

import com.benparvar.crest.exception.UsuarioException;
import com.benparvar.crest.model.adapter.UsuarioAdapter;
import com.benparvar.crest.model.dto.UsuarioDTO;
import com.benparvar.crest.model.entity.Usuario;
import com.benparvar.crest.model.repository.UsuarioRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.validation.Validator;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class UsuarioServiceTest {
    private static final LocalDate NASCIMENTO = LocalDate.now();
    private static final List<String> TELEFONES = Stream.of("(11) 94379-3198").collect(Collectors.toList());
    private static final String ENDERECO = "Rua Prof. Geraldina da Silva R. P. 582, Almerinda Chaves, Jundiaí - SP";
    private static final List<String> EMAILS = Stream.of("benparvar@gmail.com").collect(Collectors.toList());
    private static Validator validator;
    private final String VAZIO = "";
    private final String CPF = "276.193.418-03";
    private final String NOME = "Nome";
    private final String SOBRENOME = "Sobrenome";
    private final String ESPACO = " ";

    @Spy
    @InjectMocks
    private UsuarioService service;

    @Mock
    private UsuarioRepository repository;

    @Test(expected = IllegalArgumentException.class)
    public void inserirUsuarioNulo() {
        final UsuarioDTO usuario = null;

        try {
            service.inserir(usuario);
        } catch (IllegalArgumentException e) {
            assertEquals("usuário não pode ser nulo", e.getMessage());
            verify(repository, never()).saveAndFlush(any());
            throw e;
        }

        fail("Deveria lançar uma IllegalArgumentException");
    }

    @Test
    public void inserirUsuarioOk() {
        final UsuarioDTO usuario = UsuarioDTO.builder()
                .cpf(CPF)
                .nome(NOME)
                .sobrenome(SOBRENOME)
                .nascimento(NASCIMENTO)
                .telefones(TELEFONES)
                .endereco(ENDERECO)
                .emails(EMAILS)
                .build();

        service.inserir(usuario);
        assertNotNull(usuario);
        verify(repository).saveAndFlush(any());
    }

    @Test(expected = IllegalArgumentException.class)
    public void inserirLoteNulo() {
        final List<UsuarioDTO> usuarios = null;

        try {
            service.inserir(usuarios);
        } catch (IllegalArgumentException e) {
            assertEquals("lista de usuários não pode ser nula", e.getMessage());
            verify(repository, never()).saveAndFlush(any());
            throw e;
        }

        fail("Deveria lançar uma IllegalArgumentException");
    }

    @Test
    public void inserirLoteVazio() {
        final List<UsuarioDTO> usuarios = Collections.EMPTY_LIST;
        service.inserir(usuarios);

        assertNotNull(usuarios);
        verify(repository, never()).saveAndFlush(any());
    }

    @Test
    public void inserirLoteOk() {
        final List<UsuarioDTO> usuarios = new ArrayList<>();

        for (int i = 1; i < 11; i++) {
            UsuarioDTO usuario = UsuarioDTO.builder()
                    .nascimento(NASCIMENTO)
                    .telefones(TELEFONES)
                    .endereco(ENDERECO)
                    .emails(EMAILS)
                    .cpf(CPF)
                    .nome(NOME)
                    .sobrenome(SOBRENOME + ESPACO + i)
                    .build();

            usuarios.add(usuario);
        }

        service.inserir(usuarios);

        assertNotNull(usuarios);
        verify(repository, times(10)).saveAndFlush(any());
    }

    @Test(expected = IllegalArgumentException.class)
    public void excluirUsuarioNulo() throws UsuarioException {
        final UsuarioDTO usuario = null;

        try {
            service.excluir(usuario);
        } catch (IllegalArgumentException e) {
            assertEquals("usuário não pode ser nulo", e.getMessage());
            verify(repository, never()).delete((Usuario) any());
            throw e;
        }

        fail("Deveria lançar uma IllegalArgumentException");
    }

    @Test(expected = UsuarioException.class)
    public void excluirUsuarioVazio() throws UsuarioException {
        final UsuarioDTO usuario = UsuarioDTO.builder().build();

        try {
            service.excluir(usuario);
        } catch (UsuarioException e) {
            assertEquals("Usuário não encontrado", e.getMessage());
            verify(repository, never()).delete((Usuario) any());
            throw e;
        }

        fail("Deveria lançar uma UsuarioException");
    }

    @Test(expected = UsuarioException.class)
    public void excluirUsuarioInexistente() throws UsuarioException {
        final UsuarioDTO usuario = UsuarioDTO.builder()
                .nascimento(NASCIMENTO)
                .telefones(TELEFONES)
                .endereco(ENDERECO)
                .emails(EMAILS)
                .cpf(CPF)
                .nome(NOME)
                .sobrenome(SOBRENOME)
                .build();

        try {
            service.excluir(usuario);
        } catch (UsuarioException e) {
            assertEquals("Usuário não encontrado", e.getMessage());
            verify(repository, never()).delete((Usuario) any());
            throw e;
        }

        fail("Deveria lançar uma UsuarioException");

    }

    @Test
    public void excluirUsuarioExistente() throws UsuarioException {
        final UsuarioDTO usuario = UsuarioDTO.builder()
                .nascimento(NASCIMENTO)
                .telefones(TELEFONES)
                .endereco(ENDERECO)
                .emails(EMAILS)
                .cpf(CPF)
                .nome(NOME)
                .sobrenome(SOBRENOME)
                .build();

        final Usuario entidade = UsuarioAdapter.dtoToEntity(usuario);

        when(repository.findOneByCpf(CPF)).thenReturn(entidade);
        service.excluir(usuario);

        verify(repository).delete((entidade));
    }

    @Test(expected = IllegalArgumentException.class)
    public void atualizarUsuarioNulo() throws UsuarioException {
        UsuarioDTO usuario = null;

        try {
            service.atualizar(usuario);
        } catch (IllegalArgumentException e) {
            assertEquals("usuário não pode ser nulo", e.getMessage());
            verify(repository, never()).saveAndFlush(any());
            throw e;
        }

        fail("Deveria lançar uma IllegalArgumentException");
    }

    @Test(expected = UsuarioException.class)
    public void atualizarUsuarioVazio() throws UsuarioException {
        final UsuarioDTO usuario = UsuarioDTO.builder().build();
        when(repository.findOneByCpf(VAZIO)).thenReturn(null);

        try {
            service.atualizar(usuario);
        } catch (UsuarioException e) {
            assertEquals("Usuário não encontrado", e.getMessage());
            verify(repository, never()).saveAndFlush((any()));
            throw e;
        }

        fail("Deveria lançar uma UsuarioException");
    }

    @Test(expected = UsuarioException.class)
    public void atualizarUsuarioInexistente() throws UsuarioException {
        final UsuarioDTO usuario = UsuarioDTO.builder()
                .cpf(CPF)
                .nome(NOME)
                .sobrenome(SOBRENOME)
                .nascimento(NASCIMENTO)
                .telefones(TELEFONES)
                .endereco(ENDERECO)
                .emails(EMAILS)
                .build();

        when(repository.findOneByCpf(CPF)).thenReturn(null);

        try {
            service.atualizar(usuario);
        } catch (UsuarioException e) {
            assertEquals("Usuário não encontrado", e.getMessage());
            verify(repository, never()).saveAndFlush((any()));
            throw e;
        }

        fail("Deveria lançar uma UsuarioException");
    }

    @Test
    public void atualizarUsuarioExistenteValido() throws UsuarioException {
        final UsuarioDTO usuario = UsuarioDTO.builder()
                .nascimento(NASCIMENTO)
                .telefones(TELEFONES)
                .endereco(ENDERECO)
                .emails(EMAILS)
                .cpf(CPF)
                .nome(NOME)
                .sobrenome(SOBRENOME)
                .build();

        final Usuario entidade = UsuarioAdapter.dtoToEntity(usuario);

        when(repository.findOneByCpf(CPF)).thenReturn(entidade);
        service.atualizar(usuario);

        verify(repository).saveAndFlush((entidade));
    }

}