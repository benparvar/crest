package com.benparvar.crest.fixture.json;

/**
 * Usuario JSON fixtures
 */
public class UsuarioJson {
    public static String REQUISICAO_VAZIA = "{}";
    public static String REQUISICAO_LOTE_VAZIA = "[]";
    public static String REQUISICAO_INVALIDA = "{{\n" +
            "\t\"cpf\": \"276.193.418-03\",\n" +
            "\t\"nome\": \"Alan\",\n" +
            "\t\"telefones\" : [\"(11) 94379-3198\", \"(11) 4816-2453\"],\n" +
            "\t\"nascimento\": \"1980-01-10\",\n" +
            "\t\"emails\": [\"benparvar@gmail.com\"],\n" +
            "\t\"endereco\" : \"Rua Prof. Geraldina da Silva R. P. 582, Almerinda Chaves, Jundiaí - SP\"\n" +
            "}}";
    public static String REQUISICAO_OK = "{\n" +
            "\t\"cpf\": \"276.193.418-03\",\n" +
            "\t\"nome\": \"Alan\",\n" +
            "\t\"sobrenome\": \"Alencar da Silva\",\n" +
            "\t\"telefones\" : [\"(11) 94379-3198\", \"(11) 4816-2453\"],\n" +
            "\t\"nascimento\": \"1980-01-10\",\n" +
            "\t\"emails\": [\"benparvar@gmail.com\"],\n" +
            "\t\"endereco\" : \"Rua Prof. Geraldina da Silva R. P. 582, Almerinda Chaves, Jundiaí - SP\"\n" +
            "}";

    public static String REQUISICAO_LOTE_OK = "[{\t\n" +
            "\t\"cpf\": \"276.193.418-03\",\n" +
            "\t\"nome\": \"Alan\",\n" +
            "\t\"sobrenome\": \"Alencar da Silva\",\n" +
            "\t\"telefones\" : [\"(11) 94379-3198\", \"(11) 4816-2453\"],\n" +
            "\t\"nascimento\": \"1980-01-10\",\n" +
            "\t\"emails\": [\"benparvar@gmail.com\"],\n" +
            "\t\"endereco\" : \"Rua Prof. Geraldina da Silva R. P. 582, Almerinda Chaves, Jundiaí - SP\"\n" +
            "\t\n" +
            "},\n" +
            "{\t\n" +
            "\t\"cpf\": \"274.553.606-06\",\n" +
            "\t\"nome\": \"Alone\",\n" +
            "\t\"sobrenome\": \"Alencar da Silva\",\n" +
            "\t\"telefones\" : [\"(11) 94379-3112\", \"(11) 4816-2453\"],\n" +
            "\t\"nascimento\": \"1990-03-28\",\n" +
            "\t\"emails\": [\"alone@gmail.com\"],\n" +
            "\t\"endereco\" : \"Rua Prof. Geraldina da Silva R. P. 582, Almerinda Chaves, Jundiaí - SP\"\n" +
            "\t\n" +
            "}\n" +
            "]";

    public static String RESPOSTA_USUARIO_OK = "{\n" +
            "    \"cpf\": \"27619341803\",\n" +
            "    \"nome\": \"Alan\",\n" +
            "    \"sobrenome\": \"Alencar da Silva\",\n" +
            "    \"nascimento\": \"1980-01-10\",\n" +
            "    \"endereco\": \"Rua Prof. Geraldina da Silva R. P. 582, Almerinda Chaves, Jundiaí - SP\",\n" +
            "    \"telefones\": [\n" +
            "        \"(11) 94379-3198\"\n" +
            "    ],\n" +
            "    \"emails\": [\n" +
            "        \"benparvar@gmail.com\"\n" +
            "    ],\n" +
            "    \"ativo\": true\n" +
            "}";

    public static String RESPOSTA_LISTA_USUARIO_OK = "[{\n" +
            "    \"cpf\": \"27619341803\",\n" +
            "    \"nome\": \"Alan\",\n" +
            "    \"sobrenome\": \"Alencar da Silva\",\n" +
            "    \"nascimento\": \"1980-01-10\",\n" +
            "    \"endereco\": \"Rua Prof. Geraldina da Silva R. P. 582, Almerinda Chaves, Jundiaí - SP\",\n" +
            "    \"telefones\": [\n" +
            "        \"(11) 94379-3198\"\n" +
            "    ],\n" +
            "    \"emails\": [\n" +
            "        \"benparvar@gmail.com\"\n" +
            "    ],\n" +
            "    \"ativo\": true\n" +
            "}]";
}