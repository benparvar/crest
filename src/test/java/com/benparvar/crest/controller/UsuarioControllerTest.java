package com.benparvar.crest.controller;

import com.benparvar.crest.exception.UsuarioException;
import com.benparvar.crest.fixture.json.UsuarioJson;
import com.benparvar.crest.model.dto.UsuarioDTO;
import com.benparvar.crest.service.UsuarioService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.validation.ConstraintViolationException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = UsuarioController.class, secure = false)
public class UsuarioControllerTest {

    private final String CPF = "27619341803";
    private final String NOME = "Alan";
    private final String SOBRENOME = "Alencar da Silva";
    private static final LocalDate NASCIMENTO = LocalDate.parse("1980-01-10");
    private static final List<String> TELEFONES = Stream.of("(11) 94379-3198").collect(Collectors.toList());
    private static final String ENDERECO = "Rua Prof. Geraldina da Silva R. P. 582, Almerinda Chaves, Jundiaí - SP";
    private static final List<String> EMAILS = Stream.of("benparvar@gmail.com").collect(Collectors.toList());

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UsuarioService usuarioService;

    @Test
    public void inserirUsuarioVazio() throws Exception {
        doThrow(new ConstraintViolationException(Collections.emptySet()))
                .when(usuarioService).inserir((UsuarioDTO) any());

        mvc.perform(post("/api/v1/usuarios")
                .accept(MediaType.APPLICATION_JSON)
                .content(UsuarioJson.REQUISICAO_VAZIA)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void inserirUsuarioInvalido() throws Exception {
        doThrow(new ConstraintViolationException(Collections.emptySet()))
                .when(usuarioService).inserir((UsuarioDTO) any());

        mvc.perform(post("/api/v1/usuarios")
                .accept(MediaType.APPLICATION_JSON)
                .content(UsuarioJson.REQUISICAO_INVALIDA)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void inserirUsuarioOK() throws Exception {
        mvc.perform(post("/api/v1/usuarios")
                .accept(MediaType.APPLICATION_JSON)
                .content(UsuarioJson.REQUISICAO_OK)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        verify(usuarioService).inserir(any(UsuarioDTO.class));
    }

    @Test
    public void inserirLoteVazio() throws Exception {
        doThrow(new ConstraintViolationException(Collections.emptySet()))
                .when(usuarioService).inserir(anyList());

        mvc.perform(post("/api/v1/usuarios/bulk")
                .accept(MediaType.APPLICATION_JSON)
                .content(UsuarioJson.REQUISICAO_LOTE_VAZIA)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void inserirLoteOk() throws Exception {
        mvc.perform(post("/api/v1/usuarios/bulk")
                .accept(MediaType.APPLICATION_JSON)
                .content(UsuarioJson.REQUISICAO_LOTE_OK)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        verify(usuarioService).inserir(anyList());
    }

    @Test
    public void atualizarUsuarioInexistente() throws Exception {
        doThrow(new UsuarioException())
                .when(usuarioService).atualizar((any()));

        mvc.perform(put("/api/v1/usuarios/" + CPF)
                .accept(MediaType.APPLICATION_JSON)
                .content(UsuarioJson.REQUISICAO_OK)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void atualizarOk() throws Exception {
        mvc.perform(put("/api/v1/usuarios/" + CPF)
                .accept(MediaType.APPLICATION_JSON)
                .content(UsuarioJson.REQUISICAO_OK)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(usuarioService).atualizar(any(UsuarioDTO.class));
    }

    @Test
    public void excluirUsuarioInexistente() throws Exception {
        doThrow(new UsuarioException())
                .when(usuarioService).excluir(CPF);

        mvc.perform(delete("/api/v1/usuarios/" + CPF)
                .accept(MediaType.APPLICATION_JSON)
                .content(UsuarioJson.REQUISICAO_INVALIDA)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void excluirUsuarioOk() throws Exception {
        mvc.perform(delete("/api/v1/usuarios/" + CPF)
                .accept(MediaType.APPLICATION_JSON)
                .content(UsuarioJson.REQUISICAO_INVALIDA)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        verify(usuarioService).excluir(CPF);
    }

    @Test
    public void buscarNome() throws Exception {
        given(usuarioService.buscarNome(NOME)).willReturn(
                Stream.of(
                        UsuarioDTO.builder()
                                .cpf(CPF)
                                .nome(NOME)
                                .sobrenome(SOBRENOME)
                                .nascimento(NASCIMENTO)
                                .emails(EMAILS)
                                .telefones(TELEFONES)
                                .endereco(ENDERECO)
                                .build()).collect(Collectors.toList()));

        mvc.perform(get("/api/v1/usuarios/" + NOME + "/nome")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(UsuarioJson.RESPOSTA_LISTA_USUARIO_OK))
                .andReturn();

        verify(usuarioService).buscarNome(NOME);
    }

    @Test
    public void buscarSobrenome() throws Exception {
        given(usuarioService.buscarSobrenome(SOBRENOME)).willReturn(
                Stream.of(
                        UsuarioDTO.builder()
                                .cpf(CPF)
                                .nome(NOME)
                                .sobrenome(SOBRENOME)
                                .nascimento(NASCIMENTO)
                                .emails(EMAILS)
                                .telefones(TELEFONES)
                                .endereco(ENDERECO)
                                .build()).collect(Collectors.toList()));

        mvc.perform(get("/api/v1/usuarios/" + SOBRENOME + "/sobrenome")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(UsuarioJson.RESPOSTA_LISTA_USUARIO_OK))
                .andReturn();

        verify(usuarioService).buscarSobrenome(SOBRENOME);
    }

    @Test
    public void buscarCpf() throws Exception {
        given(usuarioService.buscarCpf(CPF)).willReturn(UsuarioDTO.builder()
                .cpf(CPF)
                .nome(NOME)
                .sobrenome(SOBRENOME)
                .nascimento(NASCIMENTO)
                .emails(EMAILS)
                .telefones(TELEFONES)
                .endereco(ENDERECO)
                .build());


        mvc.perform(get("/api/v1/usuarios/" + CPF + "/cpf")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(UsuarioJson.RESPOSTA_USUARIO_OK))
                .andReturn();

        verify(usuarioService).buscarCpf(CPF);
    }
}