package com.benparvar.crest.model.repository;

import com.benparvar.crest.model.entity.Usuario;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.validation.ConstraintViolationException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static junit.framework.TestCase.fail;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
                scripts = {"classpath:fixtures/usuario-fixture.sql"})
})
@TestPropertySource(locations = "classpath:application-test.properties")
public class UsuarioRepositoryTest {
    private static final Long ID = 1L;
    private static final LocalDate NASCIMENTO = LocalDate.now();
    private static final List<String> TELEFONES = Stream.of("(11) 99999-9999").collect(Collectors.toList());
    private static final String ENDERECO = "Rua Prof. Geraldina da Silva R. P. 582, Almerinda Chaves, Jundiaí - SP";
    private static final List<String> EMAILS = Stream.of("teste@gmail.com").collect(Collectors.toList());
    public static final String CPF_JA_EXISTENTE = "276.193.418-03";
    private final String VAZIO = "";
    private final String CPF = "652.107.110-64";
    private final String NOME = "Nome";
    private final String SOBRENOME = "Sobrenome";

    @Autowired
    private UsuarioRepository repository;

    @Test(expected = InvalidDataAccessApiUsageException.class)
    public void inserirUsuarioNulo() {
        final Usuario usuario = null;

        try {
            repository.saveAndFlush(usuario);
        } catch (IllegalArgumentException e) {
            assertEquals("Target object must not be null", e.getMessage());
            throw e;
        }
        fail("Deveria lançar uma InvalidDataAccessApiUsageException");
    }

    @Test(expected = ConstraintViolationException.class)
    public void inserirUsuarioVazio() {
        final Usuario usuario = Usuario.builder().build();

        try {
            repository.saveAndFlush(usuario);
        } catch (ConstraintViolationException e) {
            assertEquals(6, e.getConstraintViolations().size());
            throw e;
        }

        fail("Deveria lançar uma ConstraintViolationException");
    }

    @Test(expected = ConstraintViolationException.class)
    public void inserirCpfVazio() {
        final Usuario usuario = Usuario.builder()
                .cpf(VAZIO)
                .nome(NOME)
                .sobrenome(SOBRENOME)
                .nascimento(NASCIMENTO)
                .telefones(TELEFONES)
                .endereco(ENDERECO)
                .emails(EMAILS)
                .build();

        try {
            repository.saveAndFlush(usuario);
        } catch (ConstraintViolationException e) {
            assertEquals(1, e.getConstraintViolations().size());
            throw e;
        }

        fail("Deveria lançar uma Exceção do tipo ConstraintViolationException");
    }

    @Test(expected = ConstraintViolationException.class)
    public void inserirCpfInvalido() {
        final Usuario usuario = Usuario.builder()
                .cpf("123")
                .nome(NOME)
                .sobrenome(SOBRENOME)
                .nascimento(NASCIMENTO)
                .telefones(TELEFONES)
                .endereco(ENDERECO)
                .emails(EMAILS)
                .build();

        try {
            repository.saveAndFlush(usuario);
        } catch (ConstraintViolationException e) {
            assertEquals(1, e.getConstraintViolations().size());
            throw e;
        }

        fail("Deveria lançar uma Exceção do tipo ConstraintViolationException");
    }

    @Test(expected = ConstraintViolationException.class)
    public void inserirNomeVazio() {
        final Usuario usuario = Usuario.builder()
                .cpf(CPF)
                .nome(VAZIO)
                .sobrenome(SOBRENOME)
                .nascimento(NASCIMENTO)
                .telefones(TELEFONES)
                .endereco(ENDERECO)
                .emails(EMAILS)
                .build();

        try {
            repository.saveAndFlush(usuario);
        } catch (ConstraintViolationException e) {
            assertEquals(1, e.getConstraintViolations().size());
            throw e;
        }

        fail("Deveria lançar uma Exceção do tipo ConstraintViolationException");
    }

    @Test(expected = ConstraintViolationException.class)
    public void inserirSobrenomeVazio() {
        final Usuario usuario = Usuario.builder()
                .cpf(CPF)
                .nome(NOME)
                .sobrenome(VAZIO)
                .nascimento(NASCIMENTO)
                .telefones(TELEFONES)
                .endereco(ENDERECO)
                .emails(EMAILS)
                .build();

        try {
            repository.saveAndFlush(usuario);
        } catch (ConstraintViolationException e) {
            assertEquals(1, e.getConstraintViolations().size());
            throw e;
        }

        fail("Deveria lançar uma Exceção do tipo ConstraintViolationException");
    }

    @Test(expected = ConstraintViolationException.class)
    public void inserirNascimentoNulo() {
        final Usuario usuario = Usuario.builder()
                .cpf(CPF)
                .nome(NOME)
                .sobrenome(SOBRENOME)
                .nascimento(null)
                .telefones(TELEFONES)
                .endereco(ENDERECO)
                .emails(EMAILS)
                .build();

        try {
            repository.saveAndFlush(usuario);
        } catch (ConstraintViolationException e) {
            assertEquals(1, e.getConstraintViolations().size());
            throw e;
        }

        fail("Deveria lançar uma Exceção do tipo ConstraintViolationException");
    }

    @Test(expected = ConstraintViolationException.class)
    public void inserirTelefonesNulo() {
        final Usuario usuario = Usuario.builder()
                .cpf(CPF)
                .nome(NOME)
                .sobrenome(SOBRENOME)
                .nascimento(NASCIMENTO)
                .telefones(null)
                .endereco(ENDERECO)
                .emails(EMAILS)
                .build();

        try {
            repository.saveAndFlush(usuario);
        } catch (ConstraintViolationException e) {
            assertEquals(1, e.getConstraintViolations().size());
            throw e;
        }

        fail("Deveria lançar uma Exceção do tipo ConstraintViolationException");
    }

    @Test(expected = ConstraintViolationException.class)
    public void inserirTelefonesVazio() {
        final Usuario usuario = Usuario.builder()
                .cpf(CPF)
                .nome(NOME)
                .sobrenome(SOBRENOME)
                .nascimento(NASCIMENTO)
                .telefones(Collections.EMPTY_LIST)
                .endereco(ENDERECO)
                .emails(EMAILS)
                .build();

        try {
            repository.saveAndFlush(usuario);
        } catch (ConstraintViolationException e) {
            assertEquals(1, e.getConstraintViolations().size());
            throw e;
        }

        fail("Deveria lançar uma Exceção do tipo ConstraintViolationException");
    }

    @Test(expected = ConstraintViolationException.class)
    public void inserirEnderecoNulo() {
        final Usuario usuario = Usuario.builder()
                .cpf(CPF)
                .nome(NOME)
                .sobrenome(SOBRENOME)
                .nascimento(NASCIMENTO)
                .telefones(TELEFONES)
                .endereco(null)
                .emails(EMAILS)
                .build();

        try {
            repository.saveAndFlush(usuario);
        } catch (ConstraintViolationException e) {
            assertEquals(1, e.getConstraintViolations().size());
            throw e;
        }

        fail("Deveria lançar uma Exceção do tipo ConstraintViolationException");
    }

    @Test(expected = ConstraintViolationException.class)
    public void inserirEnderecoVazio() {
        final Usuario usuario = Usuario.builder()
                .cpf(CPF)
                .nome(NOME)
                .sobrenome(SOBRENOME)
                .nascimento(NASCIMENTO)
                .telefones(TELEFONES)
                .endereco(VAZIO)
                .emails(EMAILS)
                .build();

        try {
            repository.saveAndFlush(usuario);
        } catch (ConstraintViolationException e) {
            assertEquals(1, e.getConstraintViolations().size());
            throw e;
        }

        fail("Deveria lançar uma Exceção do tipo ConstraintViolationException");
    }

    @Test(expected = ConstraintViolationException.class)
    public void inserirEmailsNulo() {
        final Usuario usuario = Usuario.builder()
                .cpf(CPF)
                .nome(NOME)
                .sobrenome(SOBRENOME)
                .nascimento(NASCIMENTO)
                .telefones(TELEFONES)
                .endereco(ENDERECO)
                .emails(null)
                .build();

        try {
            repository.saveAndFlush(usuario);
        } catch (ConstraintViolationException e) {
            assertEquals(1, e.getConstraintViolations().size());
            throw e;
        }

        fail("Deveria lançar uma Exceção do tipo ConstraintViolationException");
    }

    @Test(expected = ConstraintViolationException.class)
    public void inserirEmailsVazio() {
        final Usuario usuario = Usuario.builder()
                .cpf(CPF)
                .nome(NOME)
                .sobrenome(SOBRENOME)
                .nascimento(NASCIMENTO)
                .telefones(TELEFONES)
                .endereco(ENDERECO)
                .emails(Collections.EMPTY_LIST)
                .build();

        try {
            repository.saveAndFlush(usuario);
        } catch (ConstraintViolationException e) {
            assertEquals(1, e.getConstraintViolations().size());
            throw e;
        }

        fail("Deveria lançar uma Exceção do tipo ConstraintViolationException");
    }

    @Test
    public void inserirUsuarioOK() {
        final Usuario usuario = Usuario.builder()
                .cpf(CPF)
                .nome(NOME)
                .sobrenome(SOBRENOME)
                .nascimento(NASCIMENTO)
                .telefones(TELEFONES)
                .endereco(ENDERECO)
                .emails(EMAILS)
                .build();

        repository.saveAndFlush(usuario);
        assertTrue(usuario.getAtivo());
    }


    @Test(expected = DataIntegrityViolationException.class)
    public void inserirCpfJaExistente() {
        final Usuario usuario = Usuario.builder()
                .cpf(CPF_JA_EXISTENTE)
                .nome(NOME)
                .sobrenome(SOBRENOME)
                .nascimento(NASCIMENTO)
                .telefones(TELEFONES)
                .endereco(ENDERECO)
                .emails(EMAILS)
                .build();

        try {
            repository.saveAndFlush(usuario);
        } catch (DataIntegrityViolationException e) {
            throw e;
        }

        fail("Deveria lançar uma Exceção do tipo DataIntegrityViolationException");
    }

    @Test
    public void findByNomeNulo() {
        List<Usuario> usuarios = repository.findByNome(null);

        assertNotNull(usuarios);
        assertTrue(usuarios.isEmpty());
        assertEquals(0, usuarios.size());
    }

    @Test
    public void findByNomeVazio() {
        List<Usuario> usuarios = repository.findByNome(VAZIO);

        assertNotNull(usuarios);
        assertTrue(usuarios.isEmpty());
        assertEquals(0, usuarios.size());
    }

    @Test
    public void findByNomeInexistente() {
        List<Usuario> usuarios = repository.findByNome("Java");

        assertNotNull(usuarios);
        assertTrue(usuarios.isEmpty());
        assertEquals(0, usuarios.size());
    }

    @Test
    public void findByNomeExistente() {
        List<Usuario> usuarios = repository.findByNome("Alan");

        assertNotNull(usuarios);
        assertFalse(usuarios.isEmpty());
        assertEquals(1, usuarios.size());
        assertThat(usuarios, containsInAnyOrder(hasProperty("nome", is("Alan"))));
    }

    @Test
    public void findBySobrenomeNulo() {
        List<Usuario> usuarios = repository.findBySobrenome(null);

        assertNotNull(usuarios);
        assertTrue(usuarios.isEmpty());
        assertEquals(0, usuarios.size());
    }

    @Test
    public void findBySobrenomeVazio() {
        List<Usuario> usuarios = repository.findBySobrenome(VAZIO);

        assertNotNull(usuarios);
        assertTrue(usuarios.isEmpty());
        assertEquals(0, usuarios.size());
    }

    @Test
    public void findBySobrenomeInexistente() {
        List<Usuario> usuarios = repository.findBySobrenome("Bacalhau da Silva");

        assertNotNull(usuarios);
        assertTrue(usuarios.isEmpty());
        assertEquals(0, usuarios.size());
    }

    @Test
    public void findBySobrenomeExistente() {
        List<Usuario> usuarios = repository.findBySobrenome("Alencar da Silva");

        assertNotNull(usuarios);
        assertFalse(usuarios.isEmpty());
        assertEquals(2, usuarios.size());
        assertThat(usuarios, containsInAnyOrder(hasProperty("nome", is("Alan")),
                hasProperty("nome", is("Alone"))));
    }

    @Test
    public void findOneByCpfNulo() {
        Usuario usuario = repository.findOneByCpf(null);

        assertNull(usuario);
    }

    @Test
    public void findOneByCpfVazio() {
        Usuario usuario = repository.findOneByCpf(VAZIO);

        assertNull(usuario);
    }

    @Test
    public void findOneByCpfInexistente() {
        Usuario usuario = repository.findOneByCpf("888.888.888-02");

        assertNull(usuario);
    }

    @Test
    public void findOneByCpfExistente() {
        Usuario usuario = repository.findOneByCpf(CPF_JA_EXISTENTE);

        assertNotNull(usuario);
        assertEquals(CPF_JA_EXISTENTE, usuario.getCpf());
    }
}