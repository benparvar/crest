package com.benparvar.crest.model.adapter;

import com.benparvar.crest.model.dto.UsuarioDTO;
import com.benparvar.crest.model.entity.CpfUtil;
import com.benparvar.crest.model.entity.Usuario;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;
import static org.junit.Assert.*;

public class UsuarioAdapterTest {
    private static final LocalDate NASCIMENTO = LocalDate.now();
    private static final List<String> TELEFONES = Stream.of("(11) 94379-3198").collect(Collectors.toList());
    private static final String ENDERECO = "Rua Prof. Geraldina da Silva R. P. 582, Almerinda Chaves, Jundiaí - SP";
    private static final List<String> EMAILS = Stream.of("benparvar@gmail.com").collect(Collectors.toList());
    private final String ESPACO = " ";
    private final String CPF = "276.193.418-03";
    private final String NOME = "Nome";
    private final String SOBRENOME = "Sobrenome";

    @Test(expected = IllegalArgumentException.class)
    public void dtoToEntityNulo() {
        UsuarioDTO dto = null;
        try {
            Usuario usuario = UsuarioAdapter.dtoToEntity(dto);
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("usuário (dto) não pode ser nulo", e.getMessage());
            throw e;
        }

        fail("Deveria lançar uma IllegalArgumentException");
    }

    @Test
    public void dtoToEntityOk() {
        UsuarioDTO dto = UsuarioDTO.builder()
                .nascimento(NASCIMENTO)
                .telefones(TELEFONES)
                .endereco(ENDERECO)
                .emails(EMAILS)
                .cpf(CPF)
                .nome(NOME)
                .sobrenome(SOBRENOME)
                .build();

        Usuario usuario = UsuarioAdapter.dtoToEntity(dto);

        assertNotNull(usuario);
        assertEquals(NASCIMENTO, usuario.getNascimento());
        assertEquals(TELEFONES, usuario.getTelefones());
        assertEquals(ENDERECO, usuario.getEndereco());
        assertEquals(EMAILS, usuario.getEmails());
        assertEquals(CpfUtil.desmascararCpf(CPF), usuario.getCpf());
        assertEquals(NOME, usuario.getNome());
        assertEquals(SOBRENOME, usuario.getSobrenome());
    }

    @Test
    public void listaDtoToEntityNula() {
        List<UsuarioDTO> dtos = null;
        List<Usuario> usuarios = UsuarioAdapter.dtoToEntity(dtos);

        assertNotNull(usuarios);
        assertTrue(usuarios.isEmpty());
    }

    @Test
    public void listaDtoToEntityVazia() {
        List<UsuarioDTO> dtos = Collections.EMPTY_LIST;
        List<Usuario> usuarios = UsuarioAdapter.dtoToEntity(dtos);

        assertNotNull(usuarios);
        assertTrue(usuarios.isEmpty());
    }

    @Test
    public void listaDtoToEntityOk() {
        List<UsuarioDTO> dtos = new ArrayList<>();

        for (int i = 1; i < 4; i++) {
            UsuarioDTO dto = UsuarioDTO.builder()
                    .nascimento(NASCIMENTO)
                    .telefones(TELEFONES)
                    .endereco(ENDERECO)
                    .emails(EMAILS)
                    .cpf(CPF)
                    .nome(NOME)
                    .sobrenome(SOBRENOME + ESPACO + i)
                    .build();

            dtos.add(dto);
        }

        List<Usuario> usuarios = UsuarioAdapter.dtoToEntity(dtos);

        assertNotNull(usuarios);
        assertFalse(usuarios.isEmpty());
        assertEquals(3, usuarios.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void entityToDtoNulo() {
        Usuario entity = null;
        try {
            UsuarioDTO usuario = UsuarioAdapter.entityToDto(entity);
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("usuário (entity) não pode ser nulo", e.getMessage());
            throw e;
        }

        fail("Deveria lançar uma IllegalArgumentException");
    }

    @Test
    public void entityToDtoOk() {
        Usuario entity = Usuario.builder()
                .nascimento(NASCIMENTO)
                .telefones(TELEFONES)
                .endereco(ENDERECO)
                .emails(EMAILS)
                .cpf(CPF)
                .nome(NOME)
                .sobrenome(SOBRENOME)
                .build();

        UsuarioDTO usuario = UsuarioAdapter.entityToDto(entity);

        assertNotNull(usuario);
        assertEquals(NASCIMENTO, usuario.getNascimento());
        assertEquals(TELEFONES, usuario.getTelefones());
        assertEquals(ENDERECO, usuario.getEndereco());
        assertEquals(EMAILS, usuario.getEmails());
        assertEquals(CPF, usuario.getCpf());
        assertEquals(NOME, usuario.getNome());
        assertEquals(SOBRENOME, usuario.getSobrenome());
    }

    @Test
    public void listaEntityToDtoNula() {
        List<Usuario> dtos = null;
        List<UsuarioDTO> usuarios = UsuarioAdapter.entityToDto(dtos);

        assertNotNull(usuarios);
        assertTrue(usuarios.isEmpty());
    }

    @Test
    public void listaEntityToDtoVazia() {
        List<Usuario> dtos = Collections.EMPTY_LIST;
        List<UsuarioDTO> usuarios = UsuarioAdapter.entityToDto(dtos);

        assertNotNull(usuarios);
        assertTrue(usuarios.isEmpty());
    }

    @Test
    public void listaEntityToDtoOk() {
        List<Usuario> entidades = new ArrayList<>();

        for (int i = 1; i < 11; i++) {
            Usuario usuario = Usuario.builder()
                    .nascimento(NASCIMENTO)
                    .telefones(TELEFONES)
                    .endereco(ENDERECO)
                    .emails(EMAILS)
                    .cpf(CPF)
                    .nome(NOME)
                    .sobrenome(SOBRENOME + ESPACO + i)
                    .build();

            entidades.add(usuario);
        }

        List<UsuarioDTO> usuarios = UsuarioAdapter.entityToDto(entidades);

        assertNotNull(usuarios);
        assertFalse(usuarios.isEmpty());
        assertEquals(10, usuarios.size());
    }
}