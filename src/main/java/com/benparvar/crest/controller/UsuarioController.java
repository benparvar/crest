package com.benparvar.crest.controller;

import com.benparvar.crest.exception.UsuarioException;
import com.benparvar.crest.model.dto.UsuarioDTO;
import com.benparvar.crest.service.UsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Usuario REST controller
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/usuarios", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value = "/api/v1/usuarios")
@Slf4j
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    public UsuarioController(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    /**
     * Insere um usuário
     *
     * @param usuario
     */
    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Cadastrar um usuario", nickname = "cadastrarUsuario")
    @ResponseStatus(HttpStatus.CREATED)
    public void inserir(@RequestBody @Valid UsuarioDTO usuario) {
        log.debug("INICIO inserir usuario={} ", usuario);
        usuarioService.inserir(usuario);
        log.debug("FIM inserir usuario={} ", usuario);
    }

    /**
     * Insere um lote de usuários
     *
     * @param usuarios
     */
    @PostMapping(value = "/bulk", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Cadastrar um lote de usuarios", nickname = "cadastrarLoteUsuarios")
    @ResponseStatus(HttpStatus.CREATED)
    public void inserirLote(@RequestBody @Valid List<UsuarioDTO> usuarios) {
        log.debug("INICIO inserir em lote usuarios={} ", usuarios);
        usuarioService.inserir(usuarios);
        log.debug("FIM inserir em lote usuarios={} ", usuarios);
    }

    /**
     * Atualiza um usuário
     */
    @PutMapping(value = "/{cpf}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Atualizar um usuario", nickname = "atualizarUsuario")
    public void atualizar(@PathVariable final String cpf, @RequestBody @Valid UsuarioDTO usuario) throws UsuarioException {
        log.debug("INICIO atualizar usuario={} ", usuario);
        usuarioService.atualizar(usuario);
        log.debug("FIM atualizar usuario={} ", usuario);
    }

    /**
     * Remove um usuário
     */
    @DeleteMapping(value = "/{cpf}")
    @ApiOperation(value = "Exclui um usuario", nickname = "excluirUsuario")
    public void excluir(@PathVariable final String cpf) throws UsuarioException {
        log.debug("INICIO excluir cpf={} ", cpf);
        usuarioService.excluir(cpf);
        log.debug("FIM excluir cpf={} ", cpf);
    }

    /**
     * Busca por nome
     */
    @GetMapping(value = "/{nome}/nome")
    @ApiOperation(value = "Busca usuarios pelo nome", nickname = "buscarUsuarioNome")
    public List<UsuarioDTO> buscarNome(@PathVariable final String nome) throws UsuarioException {
        log.debug("INICIO buscar nome={} ", nome);
        List<UsuarioDTO> usuarios = usuarioService.buscarNome(nome);
        log.debug("FIM buscar nome={} usuarios={}", nome, usuarios);
        return usuarios;
    }

    /**
     * Busca por sobrenome
     */
    @GetMapping(value = "/{sobrenome}/sobrenome")
    @ApiOperation(value = "Busca usuarios pelo sobrenome", nickname = "buscarUsuarioSobrenome")
    public List<UsuarioDTO> buscarSobrenome(@PathVariable final String sobrenome) throws UsuarioException {
        log.debug("INICIO buscar sobrenome={} ", sobrenome);
        List<UsuarioDTO> usuarios = usuarioService.buscarSobrenome(sobrenome);
        log.debug("FIM buscar sobrenome={} usuarios={}", sobrenome, usuarios);
        return usuarios;
    }

    /**
     * Busca por cpf
     */
    @GetMapping(value = "/{cpf}/cpf")
    @ApiOperation(value = "Busca usuario pelo cpf", nickname = "buscarUsuarioCpf")
    public UsuarioDTO buscarCpf(@PathVariable final String cpf) throws UsuarioException {
        log.debug("INICIO buscar cpf={} ", cpf);
        UsuarioDTO usuario = usuarioService.buscarCpf(cpf);
        log.debug("FIM buscar cpf={} usuario={}", cpf, usuario);
        return usuario;
    }

    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException ex, WebRequest request) {
        log.error(ex.getMessage());
        return new ResponseEntity<>(
                ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException ex, WebRequest request) {
        log.error(ex.getMessage());

        List<String> erros = new ArrayList<>();
        for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
            erros.add(violation.getPropertyPath() + ": " + violation.getMessage());
        }

        return new ResponseEntity<>(
                erros, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({HttpMessageNotReadableException.class})
    public ResponseEntity<Object> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex, WebRequest request) {
        log.error(ex.getMessage());
        return new ResponseEntity<>(
                "inválido: Não conseguimos entender os dados da sua requisição", new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({DataIntegrityViolationException.class})
    public ResponseEntity<Object> handleDataIntegrityViolationException(DataIntegrityViolationException ex, WebRequest request) {
        log.error(ex.getMessage());
        return new ResponseEntity<>(
                "repositório: Problema na integridade dos dados", new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({HttpRequestMethodNotSupportedException.class})
    public ResponseEntity<Object> handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException ex, WebRequest request) {
        log.error(ex.getMessage());
        return new ResponseEntity<>(
                "método: Método http não suportado", new HttpHeaders(), HttpStatus.METHOD_NOT_ALLOWED);
    }

    @ExceptionHandler({UsuarioException.class})
    public ResponseEntity<Object> handleUsuarioException(UsuarioException ex, WebRequest request) {
        log.error(ex.getMessage());
        return new ResponseEntity<>(
                ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleException(Exception ex, WebRequest request) {
        log.error(ex.getMessage());
        return new ResponseEntity<>(
                "fatal: Desculpe, não conseguimos processar sua requisição", new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
