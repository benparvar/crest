package com.benparvar.crest.service;

import com.benparvar.crest.exception.UsuarioException;
import com.benparvar.crest.model.adapter.UsuarioAdapter;
import com.benparvar.crest.model.dto.UsuarioDTO;
import com.benparvar.crest.model.entity.CpfUtil;
import com.benparvar.crest.model.entity.Usuario;
import com.benparvar.crest.model.repository.UsuarioRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * Classe referentes aos serviços do usuário
 */
@Slf4j
@Service
public class UsuarioService {
    private static final String USUARIO_NAO_PODE_SER_NULO = "usuário não pode ser nulo";
    private static final String LISTA_USUARIOS_NAO_PODE_SER_NULA = "lista de usuários não pode ser nula";
    private static final String CPF_NAO_PODE_SER_NULO = "cpf não pode ser nulo";
    public static final String USUARIO_NAO_ENCONTRADO = "Usuário não encontrado";

    @Autowired
    private UsuarioRepository repository;

    public UsuarioService(UsuarioRepository repository) {
        this.repository = repository;
    }

    /**
     * Insere um usuário
     *
     * @param usuario
     */
    @Transactional
    public void inserir(UsuarioDTO usuario) {
        Assert.notNull(usuario, USUARIO_NAO_PODE_SER_NULO);

        repository.saveAndFlush(UsuarioAdapter.dtoToEntity(usuario));
    }

    /**
     * Insere um lote de usuários
     *
     * @param usuarios
     */
    @Transactional
    public void inserir(List<UsuarioDTO> usuarios) {
        Assert.notNull(usuarios, LISTA_USUARIOS_NAO_PODE_SER_NULA);
        usuarios.forEach(this::inserir);
    }

    /**
     * Exclui um usuário
     *
     * @param usuario
     */
    @Transactional
    public void excluir(UsuarioDTO usuario) throws UsuarioException {
        Assert.notNull(usuario, USUARIO_NAO_PODE_SER_NULO);

        Usuario entidade = repository.findOneByCpf(usuario.getCpf());
        if (null == entidade)
            throw new UsuarioException(USUARIO_NAO_ENCONTRADO);

        repository.delete(entidade);
    }

    /**
     * Exclui um usuário
     *
     * @param cpfUsuario
     */
    @Transactional
    public void excluir(String cpfUsuario) throws UsuarioException {
        Assert.notNull(cpfUsuario, CPF_NAO_PODE_SER_NULO);

        Usuario entidade = repository.findOneByCpf(cpfUsuario);
        if (null == entidade)
            throw new UsuarioException(USUARIO_NAO_ENCONTRADO);

        repository.delete(entidade);
    }

    /**
     * Atualizar um usuário
     *
     * @param usuario
     */
    @Transactional
    public void atualizar(UsuarioDTO usuario) throws UsuarioException {
        Assert.notNull(usuario, USUARIO_NAO_PODE_SER_NULO);

        Usuario ent = repository.findOneByCpf(usuario.getCpf());
        if (null == ent)
            throw new UsuarioException(USUARIO_NAO_ENCONTRADO);

        Usuario entidade = UsuarioAdapter.dtoToEntity(usuario);
        entidade.setId(ent.getId());
        repository.saveAndFlush(entidade);
    }

    /**
     * Efetua a busca por nome
     *
     * @param nome
     * @return List<UsuarioDTO>
     */
    public List<UsuarioDTO> buscarNome(String nome) throws UsuarioException {
        List<Usuario> usuarios = repository.findByNome(nome);

        if (CollectionUtils.isEmpty(usuarios))
            throw new UsuarioException(USUARIO_NAO_ENCONTRADO);

        return UsuarioAdapter.entityToDto(usuarios);
    }

    /**
     * Efetua a busca por sobrenome
     *
     * @param sobrenome
     * @return List<UsuarioDTO>
     */
    public List<UsuarioDTO> buscarSobrenome(String sobrenome) throws UsuarioException {
        List<Usuario> usuarios = repository.findBySobrenome(sobrenome);

        if (CollectionUtils.isEmpty(usuarios))
            throw new UsuarioException(USUARIO_NAO_ENCONTRADO);

        return UsuarioAdapter.entityToDto(usuarios);
    }

    /**
     * Efetua a busca por cpf
     *
     * @param cpf
     * @return UsuarioDTO
     */
    public UsuarioDTO buscarCpf(String cpf) throws UsuarioException {
        Usuario usuario = repository.findOneByCpf(CpfUtil.desmascararCpf(cpf));

        if (null == usuario)
            throw new UsuarioException(USUARIO_NAO_ENCONTRADO);

        return UsuarioAdapter.entityToDto(usuario);
    }
}


