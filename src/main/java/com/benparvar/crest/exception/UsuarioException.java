package com.benparvar.crest.exception;

/**
 * Classe que representa exceções de negócio referentes ao usuário que podem ser lançadas
 */
public class UsuarioException extends Exception {
    public UsuarioException() {
        super();
    }

    public UsuarioException(String message) {
        super(message);
    }

    public UsuarioException(String message, Throwable cause) {
        super(message, cause);
    }

    public UsuarioException(Throwable cause) {
        super(cause);
    }

    protected UsuarioException(String message, Throwable cause,
                               boolean enableSuppression,
                               boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
