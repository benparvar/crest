package com.benparvar.crest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;


/**
 * Spring Boot Application class
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.benparvar.crest.*"})
@ServletComponentScan(basePackages = "**.servlet.**")
@EnableCaching
@EnableAspectJAutoProxy
@EntityScan(
        basePackageClasses = {Application.class}
)
public class Application {

    /**
     * Spring boot application main
     *
     * @param args
     */
    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
