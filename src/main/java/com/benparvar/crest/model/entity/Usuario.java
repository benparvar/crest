package com.benparvar.crest.model.entity;

import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * Classe que representa a entidade Usuario
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@ToString
public class Usuario implements Serializable {

    @Id
    @GeneratedValue
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    @CPF(message = "CPF deve ser válido")
    @Column(name = "cpf", unique = true)
    private String cpf;

    @Getter
    @Setter
    @NotEmpty(message = "Nome não pode estar vazio")
    private String nome;

    @NotEmpty(message = "Sobrenome não pode estar vazio")
    @Getter
    @Setter
    private String sobrenome;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "Nascimento não pode estar vazio")
    @Getter
    @Setter
    private LocalDate nascimento;

    @NotEmpty(message = "Endereço não pode estar vazio")
    @Getter
    @Setter
    private String endereco;

    @ElementCollection
    @NotEmpty(message = "Telefones não pode estar vazio")
    @Getter
    @Setter
    private List<String> telefones;

    @ElementCollection
    @NotEmpty(message = "Emails não pode estar vazio")
    @Getter
    @Setter
    private List<String> emails;

    @NotNull
    @Getter
    @Setter
    private Boolean ativo;

    public static class UsuarioBuilder {
        private Boolean ativo = Boolean.TRUE;
    }
}
