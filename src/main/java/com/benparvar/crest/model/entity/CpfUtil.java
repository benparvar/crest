package com.benparvar.crest.model.entity;

import org.springframework.util.Assert;

/**
 * Classe que possui (pelo menos deveria) métodos comuns referente ao CPF
 */
public class CpfUtil {
    private static final String CPF_NAO_PODE_SER_NULO = "CPF não pode ser nulo";

    public static String desmascararCpf(String cpf) {
        Assert.notNull(cpf, CPF_NAO_PODE_SER_NULO);
        return cpf.replaceAll("[.-]", "");
    }
}
