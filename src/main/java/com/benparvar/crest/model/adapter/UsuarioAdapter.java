package com.benparvar.crest.model.adapter;

import com.benparvar.crest.model.dto.UsuarioDTO;
import com.benparvar.crest.model.entity.CpfUtil;
import com.benparvar.crest.model.entity.Usuario;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Classe que representa o adapter da entidade Usuario
 */
public class UsuarioAdapter {
    private static final String DTO_NAO_PODE_SER_NULO = "usuário (dto) não pode ser nulo";
    private static final String ENTITY_NAO_PODE_SER_NULO = "usuário (entity) não pode ser nulo";

    /**
     * @param objeto
     * @return
     */
    public static Usuario dtoToEntity(UsuarioDTO objeto) {
        Assert.notNull(objeto, DTO_NAO_PODE_SER_NULO);

        Boolean ativo = objeto.getAtivo() == null ? Boolean.TRUE : objeto.getAtivo();

        return Usuario.builder()
                .emails(objeto.getEmails())
                .telefones(objeto.getTelefones())
                .endereco(objeto.getEndereco())
                .nascimento(objeto.getNascimento())
                .ativo(ativo)
                .sobrenome(objeto.getSobrenome())
                .nome(objeto.getNome())
                .cpf(CpfUtil.desmascararCpf(objeto.getCpf()))
                .build();
    }

    /**
     * @param objeto
     * @return
     */
    public static UsuarioDTO entityToDto(Usuario objeto) {
        Assert.notNull(objeto, ENTITY_NAO_PODE_SER_NULO);
        return UsuarioDTO.builder()
                .emails(objeto.getEmails())
                .telefones(objeto.getTelefones())
                .endereco(objeto.getEndereco())
                .nascimento(objeto.getNascimento())
                .ativo(objeto.getAtivo())
                .sobrenome(objeto.getSobrenome())
                .nome(objeto.getNome())
                .cpf(objeto.getCpf())
                .build();
    }

    /**
     * @param objeto
     * @return
     */
    public static List<UsuarioDTO> entityToDto(final Collection<Usuario> objeto) {
        if (objeto == null) {
            return new ArrayList<>();
        }
        return objeto.stream().map(UsuarioAdapter::entityToDto).collect(Collectors.toList());
    }

    /**
     * @param objeto
     * @return
     */
    public static List<Usuario> dtoToEntity(final Collection<UsuarioDTO> objeto) {
        if (objeto == null) {
            return new ArrayList<>();
        }
        return objeto.stream().map(UsuarioAdapter::dtoToEntity).collect(Collectors.toList());
    }

}
