package com.benparvar.crest.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

/**
 * Classe que representa um Usuario
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class UsuarioDTO {
    @Getter
    @Setter
    private String cpf;

    @Getter
    @Setter
    private String nome;

    @Getter
    @Setter
    private String sobrenome;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Getter
    @Setter
    private LocalDate nascimento;

    @Getter
    @Setter
    private String endereco;

    @Getter
    @Setter
    private List<String> telefones;

    @Getter
    @Setter
    private List<String> emails;

    @Getter
    @Setter
    private Boolean ativo;

    public static class UsuarioDTOBuilder {
        private Boolean ativo = Boolean.TRUE;
    }
}
