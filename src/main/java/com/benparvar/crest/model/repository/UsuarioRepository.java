package com.benparvar.crest.model.repository;

import com.benparvar.crest.model.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Interface referente ao repositório do usuário
 */
public interface UsuarioRepository extends JpaRepository<Usuario, String> {
    
    /**
     * @param nome
     * @return
     */
    List<Usuario> findByNome(final String nome);


    /**
     * @param sobrenome
     * @return
     */
    List<Usuario> findBySobrenome(final String sobrenome);


    /**
     * @param cpf
     * @return
     */
    Usuario findOneByCpf(final String cpf);

}
