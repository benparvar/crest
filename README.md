# crest

A very simple (MVC) CRUD.

## Requirements
* Java SE 8
* Gradle 3.2.1

## Frameworks
* Springboot
* Guava
* Lombok

## Database
* H2  "in memorian" database

## Building Project

    gradle clean build

## Unit Test

    gradle test

## Docker
* Generating the image
    ./gradlew buildDocker
* Running the image
    docker run -p 9876:9876 benparvar/crest:0.1

## Run Application

    gradle -Penv="[ENV]" bootRun


ENV: "dev", "qa", "staging" or "prod"

Default is "dev" (if "env" parameter is not defined)


## REST Documentation

Please see the link http://{server}:9876/swagger-ui.html



